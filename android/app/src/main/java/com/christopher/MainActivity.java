package com.christopher;
import android.os.Bundle;
import com.facebook.react.ReactActivity;
import com.fabricio.vergal.RNWorkers.RNWorkersManager;
import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RNWorkersManager.getInstance().startWorkers();
        super.onCreate(savedInstanceState);
    }
}
