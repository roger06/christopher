'use strict'

import { combineReducers } from 'redux'
import settings from '../screens/Settings/modules/Settings'
import progress from '../screens/Progress/modules/Progress'

export default combineReducers({
  settings,
  progress
})