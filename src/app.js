import React from 'react'
import { Provider } from 'react-redux'
import { Navigation } from 'react-native-navigation'

import registerScreens from './navigation'
import configureStore from './store/configureStore'

const store = configureStore()

registerScreens(store, Provider)

const navigatorStyle = {
  statusBarColor: 'white',
	statusBarTextColorScheme: 'light',
	navigationBarColor: 'transparent',
	navBarBackgroundColor: '#0a0a0a',
	navBarTextColor: '#FFF',
  navBarButtonColor: '#FFF',
}

Navigation.startSingleScreenApp({
	screen: {
		screen: 'main.Home',
		navigatorStyle
	}
})