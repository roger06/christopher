import Reactotron from 'reactotron-react-native'
import { reactotronRedux as reduxPlugin } from 'reactotron-redux'


if (__DEV__) {
  // More info .. https://github.com/infinitered/reactotron 
  Reactotron
    .configure({ name: 'Christopher' })
    .useReactNative()
    .connect({
       enabled: __DEV__,
       name: 'Christopher',
    })

  Reactotron.log('Init')
  console.disableYellowBox = true
  global.console.rec = Reactotron

  global.console.rec.logs = (title, log) => {
    if (title && log) {
      console.rec.display({
         name: 'Christopher',
         preview: title,
         value: log,
         important: true
       })
    } else {
      console.rec.log(title);
    }
  }
} else {
  global.console.rec = {}
}