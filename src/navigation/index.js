'use strict'
import { Navigation } from 'react-native-navigation'

import Home from '../screens/Home'
import Settings from '../screens/Settings'
import Progress from '../screens/Progress'
import Result from '../screens/Result'

/**
 * Register and inject the redux store to all the scenes/screens/parts in the navigator
 * @param {Object} store: Redux Store
 * @param {Object} Provider: Redux Provider
 */
export default (store, Provider) => {
  Navigation.registerComponent('main.Home', () => Home, store, Provider)
  Navigation.registerComponent('main.Settings', () => Settings, store, Provider)
  Navigation.registerComponent('main.Progress', () => Progress, store, Provider)
  Navigation.registerComponent('main.Result', () => Result, store, Provider)
}