import { applyMiddleware, compose, createStore } from 'redux'
import RootReducer from '../reducers/RootReducer'
import Reactotron from 'reactotron-react-native'
import createTrackingEnhancer from 'reactotron-redux'
import thunk from 'redux-thunk'


export default (initialState = {}) => {
  const tracker = __DEV__ ? createTrackingEnhancer(Reactotron, {}) : {}

  // ======================================================
  // Middleware Configuration
  // ======================================================
  // 
  const middleware = [thunk]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = __DEV__ ? [tracker] : []

  // ======================================================
  // Store Instantiation
  // ======================================================
  const store = createStore(
    RootReducer,
    initialState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  //Reactotron.addReduxStore(store)

  return store
}