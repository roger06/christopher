'use strict'
import { StyleSheet } from 'react-native'


/**
 * Styles fro HomeComponent
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#673AB7',
    padding: 30,
  },

  content: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50
  },

  text: {
    fontSize: 18,
    color: '#FFF',
    marginVertical: 35,
    textAlign: 'center'
  }
})

export default styles