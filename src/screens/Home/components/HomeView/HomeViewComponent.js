import React from 'react'
import { Container, Content, Button, Body, Text, Thumbnail } from 'native-base'
import { Image, StyleSheet } from 'react-native'
import styles from './styles'

const MAIN_LOGO = require('../../../../images/logo.png')

class HomeComponent extends React.Component {

  componentWillMount() {
    this.props.navigator.setStyle({
      topBarElevationShadowEnabled: false,
      statusBarColor: '#512DA8',
      navBarBackgroundColor: '#673AB7',
      navigationBarColor: '#673AB7'
    })
  }

  _start = () => {
    this.props.navigator.push({
      screen: 'main.Settings',
      title : 'Settings'
    });
  }

  render() {
    return (
      <Container style={ StyleSheet.flatten(styles.container) }>
        <Content contentContainerStyle={ StyleSheet.flatten(styles.content) }>
          <Image source={MAIN_LOGO} width={120} />
          <Text style={ StyleSheet.flatten(styles.text) }>This application makes use of the <Text style={{ fontWeight: 'bold', color: '#FFF'}}>scipher</Text> module to decode a text that has been encoded with a simple substitution cipher..</Text>
          <Button light block onPress={ this._start }><Text> START </Text></Button>
        </Content>
      </Container>
    )
  }
}

export default HomeComponent
