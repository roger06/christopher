import { connect } from 'react-redux'
import ResultView from '../components/ResultView'

const mapStateToProps = (state) => {
  return {
    file       : state.settings.file,
    result     : state.progress.result
  }
}

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(ResultView)