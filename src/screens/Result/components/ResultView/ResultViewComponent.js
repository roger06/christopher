'use strict'
import React from 'react'
import { Share } from 'react-native'
import { Container, Content, Text, Item, Footer, FooterTab, Button } from 'native-base'
import scipher from 'scipher'

import styles from './styles'

class ResultViewComponent extends React.Component {
  componentWillUpdate() {
    this.props.navigator.setStyle({
      topBarElevationShadowEnabled: true,
      statusBarColor: '#512DA8',
      navBarBackgroundColor: '#673AB7',
      navigationBarColor: '#000000',
      navBarButtonColor: '#FFFFFF',
      navBarTextColor: '#FFFFFF',
      navBarTextFontSize: 18,
    })
    this.props.navigator.setTitle({
      title: 'Results',
    })
  }

  _shareResult = (result) => {
    Share.share({
      message: result,
      title  : 'Result' 
    })
  }

  render() {
    const { result, file } = this.props
    const decodedText = scipher.decode(file, result.key);
    return (
      <Container>
        <Item style={{ padding: 10, backgroundColor: '#FFF' }} key="result-item">
          <Text>Current Key:  { result.key }</Text>
        </Item>
        <Content style={{ backgroundColor: '#F2F2F2', padding: 15 }}>
          <Text>{ decodedText }</Text>
        </Content>

        <Footer>
          <FooterTab>
            <Button full style={{ backgroundColor: '#673AB7' }} onPress={() => { this._shareResult(decodedText) }}>
              <Text>Send</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

export default ResultViewComponent
