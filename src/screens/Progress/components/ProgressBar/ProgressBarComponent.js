import React from 'react'
import { View } from 'react-native'

export const ProgressBarComponent = ({ value }) => {
  let fullPart = 0
  let emptyPart = 1

  fullPart  = value / 100
  emptyPart = 1 - fullPart

  return(
    <View style={{ height: 20, backgroundColor: '#f1d8b2', flex: 1, flexDirection: 'row' }}>
      <View style={{ height: 20, flex: fullPart, backgroundColor: '#c75352'}} />
      <View style={{ flex: emptyPart }} />
    </View>
  )
}

export default ProgressBarComponent