import React from 'react'
import { View, StyleSheet } from 'react-native'
import {Container, Content, Text, Item, Toast, Root, Footer, FooterTab, Button, Spinner} from 'native-base'
import { Worker } from 'rn-workers'
import ProgressBar from '../ProgressBar'
import scipher from 'scipher'

import styles from './styles'

class ProgressViewComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      result : {
        iteration: 1,
        score: 0,
        key: '',
        plaintext: ''
      }
    }
  }

  componentWillMount() {
    this.props.navigator.setStyle({
      topBarElevationShadowEnabled: true,
      statusBarColor: '#512DA8',
      navBarBackgroundColor: '#673AB7',
      navigationBarColor: '#000',
      navBarTextColor: '#FFF',
      navBarButtonColor: '#FFF',
    })

    

    var ngrams = require('scipher/data/triagrams.json')
    var source = scipher.extractFragment(this.props.file, this.props.numberLines)

    let config = {
      source : source,
      ngrams : ngrams,
      iterations: this.props.iterations,
      score  : 2
    }

    config = JSON.stringify(config)
    
    this.worker = new Worker()
    this.worker.onmessage = (result) => {
      let resultObject = JSON.parse(result) 
      console.rec.logs(':: Partial Result ::', resultObject)
      
      this.props.setResult(resultObject)
      this.setState({
        result: resultObject
      })

      if (resultObject.finished) {
        Toast.show({
          text: 'The process has been completed',
          position: 'bottom',
          buttonText: 'Ok'
        })
        this.worker.terminate()
      }
    }

    this.worker.postMessage(config)
  }

  componentWillUnmount() {
    this.worker.terminate()
  }

  _goToResults = () => {
    this.worker.terminate()
    this.props.navigator.push({
      screen: 'main.Result',
      title: 'Result'
    })
  }

  render() {
    const { iterations } = this.props
    const { result } = this.state
    let percent = (result.iteration / iterations) * 100

    return (
      <Root>
      <Container style={ StyleSheet.flatten(styles.contianer) }>
        <View style={ styles.resultInfo }>
          <Item style={ StyleSheet.flatten(styles.item) }>
            <Text style={{ margin: 5 }}>{ parseFloat(percent).toFixed(2) }%</Text>
            <ProgressBar value={ percent } />
          </Item>
          <Item style={ StyleSheet.flatten(styles.item) }>
            <Text>{ `Iterations Complete:  ${result.iteration}/${iterations} `}</Text>
          </Item>
          <Item style={ StyleSheet.flatten(styles.item) }>
            <Text>Best Scroe : { result.score } </Text>
          </Item>
          <Item style={ StyleSheet.flatten(styles.item) }>
            <Text>Current Key: { result.key } </Text>
          </Item>
          <Item style={ StyleSheet.flatten(styles.item) }>
            <Text>Plain Text: </Text>
          </Item>
        </View>
        <Content style={ StyleSheet.flatten(styles.content) }>
          { result.key == '' ? 
            <Spinner color="#c75352" /> :
            <Text>{ result.plaintext }</Text>
          }
        </Content>
        
        { 
          result.key != '' &&
            <Footer>
              <FooterTab>
                <Button full style={{ backgroundColor: '#673AB7' }} onPress={ this._goToResults }>
                  <Text>Finish</Text>
                </Button>
              </FooterTab>
            </Footer>
        }
      </Container>
      </Root>
    )
  }
}

export default ProgressViewComponent
