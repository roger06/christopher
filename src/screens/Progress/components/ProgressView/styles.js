'use strict'
import { StyleSheet } from 'react-native'

/**
 * Styles fro ProgressView
 */
const styles = StyleSheet.create({
  container: { 
    backgroundColor: '#F2F2F2',
    paddingBottom: 50
  },

  resultInfo : {
    backgroundColor: '#FFF', 
    flex: 0 
  },

  item: {
    padding: 10
  },

  content: {
    paddingHorizontal: 30, 
    paddingTop: 15,
    marginBottom: 0, 
    flex: 1,
  }
})

export default styles