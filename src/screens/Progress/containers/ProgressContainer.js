import { connect } from 'react-redux'
import ProgressView from '../components/ProgressView'
import { actions } from '../modules/Progress'

const mapStateToProps = (state) => {
  return {
    file        : state.settings.file,
    iterations  : state.settings.iterations,
    numberLines : state.settings.numberLines,
    results     : state.progress.results
  }
}

const mapDispatchToProps = actions

export default connect(mapStateToProps, mapDispatchToProps)(ProgressView)