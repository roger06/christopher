'use strict'

// ------------------------------------
// Constants
// ------------------------------------
export const SET_RESULTS = 'SET_RESULTS'

// ------------------------------------
// Actions
// ------------------------------------
export const actions = {
  setResult: (result) => {
    return (dispatch, getState) => {
      dispatch({
        type: SET_RESULTS,
        payload: {
          result: result
        }
      })
    }
  },
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_RESULTS]: (state, action) => { 
    return {
      ...state,
      result: action.payload.result
    }
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  result: {}
}

export default function objectCreateReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
