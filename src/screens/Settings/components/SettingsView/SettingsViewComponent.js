'use strict'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Container, Text, Content, Form, Item, Input, Radio, Right, ListItem, Footer, FooterTab, Button, Spinner} from 'native-base'

import styles from './styles'

class SettingsViewComponent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      iterations  : props.iterations,
      numberLines : props.numberLines,
      url         : props.url
    }
  }


  componentWillMount() {
    this.props.navigator.setStyle({
      topBarElevationShadowEnabled: true,
      statusBarColor: '#512DA8',
      navBarBackgroundColor: '#673AB7',
      navigationBarColor: '#000000',
      navBarTextColor: '#FFFFFF',
      navBarButtonColor: '#FFFFFF',
      navBarTextFontSize: 18,
    })

    this.props.navigator.setTitle({
      title: 'Settings'
    })
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loading && !nextProps.loading && nextProps.file) {
      this.props.navigator.push({
        screen: 'main.Progress',
      });
    }
  }

  _onChangeText = (field, value) => {
    this.setState({
      [field]: value
    })
  }

  _selectNumberLines = (lines) => {
    this.setState({
      numberLines: lines
    })
  }

  _loadSettings = () => {
    if (this.props.loading) return

    const { url, iterations, numberLines } = this.state
    this.props.loadSettings(url, iterations, numberLines)
  }

  render() {
    const { numberLines, url, iterations } = this.state
    const { loading } = this.props
    
    return (
      <Container style={ StyleSheet.flatten(styles.container) }>
        <Content style={ StyleSheet.flatten(styles.content) }>
          <Text style={ StyleSheet.flatten(styles.label) }>Url of the .txt file</Text>
          <Item regular style={ StyleSheet.flatten(styles.item) }>
            <Input 
              autoCapitalize="none"
              value={ url } 
              style={ StyleSheet.flatten(styles.input) } 
              onChangeText={(value) => { this._onChangeText('url', value) }} 
            />
          </Item>

          <Text style={ StyleSheet.flatten(styles.label) }>Iterations</Text>
          <Item regular style={ StyleSheet.flatten(styles.item) }>
            <Input 
              value={ iterations.toString() } 
              style={ StyleSheet.flatten(styles.input) } 
              onChangeText={(value) => { this._onChangeText('iterations', value) }} 
            />
          </Item>

          <Text style={ StyleSheet.flatten(styles.labelBorder) }>Number of lines to take </Text>
          <ListItem regular>
            <Text>50 lines</Text>
            <Right>
              <Radio selected={ numberLines == '50'} onPress={() => { this._selectNumberLines(50) }}/>
            </Right>
          </ListItem>

          <ListItem regular>
            <Text>100 lines</Text>
            <Right>
              <Radio selected={ numberLines == '100'} onPress={() => { this._selectNumberLines(100) }}/>
            </Right>
          </ListItem>

          <ListItem regular>
            <Text>150 lines</Text>
            <Right>
              <Radio selected={ numberLines == '150'} onPress={() => { this._selectNumberLines(150) }}/>
            </Right>
          </ListItem>
        </Content>

        <Footer>
          <FooterTab>
            <Button full style={{ backgroundColor: '#673AB7' }} onPress={ this._loadSettings }>
              <Text>START DECODING</Text>
            </Button>
          </FooterTab>
        </Footer>

        { 
          loading && 
            <View style={ styles.loading }>
              <Spinner color="#c75352" />
          </View>
        }
      </Container>
    )
  }
}

export default SettingsViewComponent;
