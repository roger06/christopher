'use strict'
import { StyleSheet } from 'react-native'


/**
 * Styles fro SettingsView
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F2F2F2',
  },

  loading : {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255,255,255,0.6)',
    justifyContent: 'center',
    alignItems: 'center'
  },

  content: {
    padding: 15,
  },

  item: {
    marginBottom: 35
  },

  input: {
    backgroundColor: '#FFF'
  },

  label: {
    color: '#777',
    paddingBottom: 5,
  },

  labelBorder: {
    color: '#777',
    borderBottomColor: '#CFCFCF',
    paddingBottom: 10,
    borderBottomWidth: 1,
  }

})

export default styles