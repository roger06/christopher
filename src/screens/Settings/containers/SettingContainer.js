import { connect } from 'react-redux'
import SettingsView from '../components/SettingsView'
import { actions } from '../modules/Settings'

const mapStateToProps = (state) => {
  return {
    file       : state.settings.file,
    loading    : state.settings.loading,
    url        : state.settings.url,
    iterations : state.settings.iterations,
    numberLines: state.settings.numberLines
  }
}

const mapDispatchToProps = actions

export default connect(mapStateToProps, mapDispatchToProps)(SettingsView)