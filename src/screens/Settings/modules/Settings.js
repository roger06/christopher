'use strict'

// ------------------------------------
// Constants
// ------------------------------------
export const LOAD_FILE = 'LOAD_FILE'
export const LOAD_FILE_SUCCESS = 'LOAD_FILE_SUCCESS'
export const LOAD_FILE_ERROR = 'LOAD_FILE_ERROR'

// ------------------------------------
// Actions
// ------------------------------------
export const actions = {
  loadSettings: (url, iterations, numberLines) => {
    return (dispatch, getState) => {
      dispatch({
        type: LOAD_FILE,
      })

      fetch(url)
        .then(response => {
          console.rec.display({
            name: 'Settings: loadSettings',
            preview: 'Response',
            value: response._bodyText
          })
          dispatch({
            type: LOAD_FILE_SUCCESS,
            payload: {
              file: response._bodyText,
              iterations: iterations,
              numberLines: numberLines
            }
          })
        })
        .catch(error => {
          console.rec.error(error)
          dispatch({
            type: LOAD_FILE_ERROR,
          })
        })
    }
  },
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [LOAD_FILE]    : (state, action) => { 
    return {
      ...state,
      loading: true
    }
  },

  [LOAD_FILE_SUCCESS]:(state, action) => {
    return {
      ...state,
      loading    : false,
      iterations : action.payload.iterations,
      numberLines: action.payload.numberLines,
      file       : action.payload.file
    }
  },

  [LOAD_FILE_ERROR]: (state, action) => {
    return {
      ...state,
      loading: false
    }
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  file   : false,
  loading: false,
  url    : 'https://goo.gl/gpLVpo',
  iterations : 300,
  numberLines: 50,
}

export default function objectCreateReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
