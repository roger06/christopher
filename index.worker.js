import { WorkerService } from 'rn-workers'
const scipher = require('scipher')
//import scipher from 'scipher'
const worker = new WorkerService();


worker.onmessage = message => {
    let config = JSON.parse(message)
    let result = scipher.breaktext(config.source, config.ngrams, config.iterations, config.score, (result) => {
        result.finished = false
        worker.postMessage(JSON.stringify(result))     
    });
    
    result.finished = true
    worker.postMessage(JSON.stringify(result))     
};