# Simple Sustition Cipher App

(This app was made as a practice for WhitePrompt_.)

Using the package previously made ([sciper](https://bitbucket.org/roger06/scipher)), a react-native application was made to show how it works.

### To make the application, the following tools/libs were used:
* For navigation and routing: [react-native-navigation](https://wix.github.io/react-native-navigation/#/)
* For state managment: [redux](https://github.com/reactjs/redux)
* For ui modules: [native-base](http://nativebase.io/) 
* For logging and debugging: [reactotron](https://github.com/infinitered/reactotron)
* For dot env configuration file: [react-native-config](https://github.com/luggit/react-native-config)
* For worker service: [rn-workers](https://github.com/fabriciovergal/react-native-workers)

### Notes
* Because the decoding process is very heavy, this was done inside a worker.
* Currently only the android version has been tested

## Requirements
You need to have installed and configured react-native cli and all its dependencies
https://facebook.github.io/react-native/docs/getting-started.html

## Installation
```bash
$ yarn  # Install project dependencies (or `npm install`)
$ node node_modules/react-native/local-cli/cli.js bundle --dev false --assets-dest ./android --entry-file index.worker.js --platform android --bundle-output ./android/app/src/main/assets/index.worker.bundle --sourcemap-output ./android/app/src/main/assets/index.worker.map # To generate the android index.worker.jsbundle
$ react-native run-android
```